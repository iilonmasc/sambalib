package sysutil

import (
	"fmt"
	"testing"

	"gitlab.com/iilonmasc/sambalib/v3/stringutil"
)

func TestFuncName(t *testing.T) {
	fn := stringutil.SurroundString(FuncName(), '[')
	cases := []struct {
		description string
		want        string
	}{
		{"Returns 'TestFuncName'", "TestFuncName"},
	}
	for _, c := range cases {
		got := FuncName()
		if got != c.want {
			t.Errorf("%q %q FuncName() == %q, want %q", fn, c.description, got, c.want)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}
