# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.1.1](https://gitlab.com/sambadevi/sambalib/compare/v3.1.0...v3.1.1) (2019-07-18)


### Bug Fixes

* **logutil.logger:** add variadic parameters to *Logger.Info/Warning/Critical functions ([12f640e](https://gitlab.com/sambadevi/sambalib/commit/12f640e))



## [3.1.0](https://gitlab.com/sambadevi/sambalib/compare/v3.0.4...v3.1.0) (2019-07-18)


### Features

* **stringutil:** add Concatenate(s []string) function to concatenate multiple strings ([f4dec94](https://gitlab.com/sambadevi/sambalib/commit/f4dec94))



### [3.0.4](https://gitlab.com/sambadevi/sambalib/compare/v3.0.3...v3.0.4) (2019-07-18)


### Bug Fixes

* **logutil.logger:** add "1" exit codes to internal usage of *Logger.CheckCritical ([6f8e0d9](https://gitlab.com/sambadevi/sambalib/commit/6f8e0d9))



### [3.0.3](https://gitlab.com/sambadevi/sambalib/compare/v3.0.2...v3.0.3) (2019-07-18)


### Bug Fixes

* **logutil.logger:** add exit param and os.Exit to *Logger.CheckCritical to actually exit with a non ([868ecd0](https://gitlab.com/sambadevi/sambalib/commit/868ecd0))



### [3.0.2](https://gitlab.com/sambadevi/sambalib/compare/v3.0.1...v3.0.2) (2019-07-15)


### Bug Fixes

* expose CheckWarning/Critical/Info functions ([4948e5b](https://gitlab.com/sambadevi/sambalib/commit/4948e5b))



## [3.0.0](https://gitlab.com/sambadevi/sambalib/compare/v2.0.2...v3.0.0) (2019-07-15)


### Bug Fixes

* add check for missing logger.rune ([64c92fe](https://gitlab.com/sambadevi/sambalib/commit/64c92fe))


### Features

* refactor back into submodules ([d850883](https://gitlab.com/sambadevi/sambalib/commit/d850883))


### BREAKING CHANGES

* Module path is now v3; functions are grouped in packages by type; signatures
changed to return errors as well



## [3.0.0](https://gitlab.com/sambadevi/sambalib/compare/v2.0.2...v3.0.0) (2019-07-15)


### Features

* refactor back into submodules ([d850883](https://gitlab.com/sambadevi/sambalib/commit/d850883))


### BREAKING CHANGES

* Module path is now v3; functions are grouped in packages by type; signatures
changed to return errors as well


##  (2019-06-28)


### Bug Fixes

* update package refs ([36d427b](https://gitlab.com/sambadevi/sambalib/commit/36d427b))


### Code Refactoring

* refactor all submodules into sambalib package ([44d45cc](https://gitlab.com/sambadevi/sambalib/commit/44d45cc))


### Features

* **stringlib:** add StrToBase64 and StrFromBase64 functions ([117c599](https://gitlab.com/sambadevi/sambalib/commit/117c599))
* **timeconverter:** split up convert function ([715ad7c](https://gitlab.com/sambadevi/sambalib/commit/715ad7c))
* add separete Check<LogLevel> functions ([a3a207e](https://gitlab.com/sambadevi/sambalib/commit/a3a207e))


### BREAKING CHANGES

* any submodule is non-existent now, import gitlab.com/sambadevi/sambalib instead of
each single submodule
