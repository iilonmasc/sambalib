package stringutil

import (
	"encoding/base64"
	"strings"
)

func BytesToString(data []byte) string {
	return string(data[:])
}

func Concatenate(s []string) string{
	var msg string
	for _,message := range s{
		msg += message + " "
	}
	msg = strings.TrimRight(msg, " ")
	return msg
}
func SurroundString(text string, character rune) string {
	start := character
	end := character
	for _, bracket := range []rune{'<', '>', '(', ')', '[', ']', '{', '}'} {
		if character == bracket {
			// map start bracket and end bracket
			if character == '>' {
				start = '<'
			}
			if character == ')' {
				start = '('
			}
			if character == ']' {
				start = '['
			}
			if character == '}' {
				start = '{'
			}
			if character == '<' {
				end = '>'
			}
			if character == '(' {
				end = ')'
			}
			if character == '[' {
				end = ']'
			}
			if character == '{' {
				end = '}'
			}
		}
	}
	return string(start) + text + string(end)
}

func StrFromBase64(baseString string) (string, error) {
	decodedString, err := base64.StdEncoding.DecodeString(baseString)
	return BytesToString(decodedString), err
}
func StrToBase64(plainString string) string {
	byteString := []byte(plainString)
	encodedString := base64.StdEncoding.EncodeToString(byteString)
	return encodedString
}
