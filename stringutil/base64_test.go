package stringutil

import (
	"fmt"
	"gitlab.com/iilonmasc/sambalib/v3/sysutil"
	"testing"
)

func TestDecode(t *testing.T) {
	fn := SurroundString(sysutil.FuncName(), '[')
	cases := []struct {
		description string
		in          string
		want        string
	}{
		{"Decode string 'test'", "dGVzdA==", "test"},
		{"Decode empty string", "", ""},
	}
	for _, c := range cases {
		got, _ := StrFromBase64(c.in)

		if got != c.want {
			t.Errorf("%q %q StrFromBase64(%q) == %q, want %q", fn, c.description, c.in, got, c.want)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}
func TestEncode(t *testing.T) {
	fn := SurroundString(sysutil.FuncName(), '[')
	cases := []struct {
		description string
		in          string
		want        string
	}{
		{"Encode string 'test'", "test", "dGVzdA=="},
		{"Encode empty string", "", ""},
	}
	for _, c := range cases {
		got := StrToBase64(c.in)
		if got != c.want {
			t.Errorf("%q %q StrToBase64(%q) == %q, want %q", fn, c.description, c.in, got, c.want)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}
