package stringutil

import (
	"errors"
	"strconv"
	"strings"
	"unicode"
)

func ConvertTime(time string) (int, error) {
	sum := 0
	var err error
	durations := SplitTime(time)
	if len(durations) > 0 {
		for _, entry := range durations {
			value, err := CalculateTime(entry)
			if err != nil{
				sum = 0
				break
			}
			sum += value
		}
	} else {
		sum = 0
	}
	return sum, err
}
func SplitTime(time string) []string {
	var length, unit string
	var durations []string
	unitBorder := false
	for _, character := range time {
		if unicode.IsDigit(character) && character != ' ' {
			if unitBorder {
				durations = append(durations, length+unit)
				length = ""
				unit = ""
				unitBorder = false
			}
			length += string(character)
		} else if character != ' ' {
			unit += string(character)
			unitBorder = true
		}
	}
	durations = append(durations, length+unit)
	return durations
}
func CalculateTime(time string) (int, error) {
	length := ""
	unit := ""
	for _, character := range time {
		if unicode.IsDigit(character) {
			length += string(character)
		} else {
			unit += string(character)
		}
	}
	unit = strings.ToLower(unit)
	duration, err := strconv.Atoi(length)

	switch unit {
	case "ms":
		break
	case "s":
		duration = duration * 1000
	case "m":
		duration = duration * 1000 * 60
	case "h":
		duration = duration * 1000 * 60 * 60
	case "d":
		duration = duration * 1000 * 60 * 60 * 24
	default:
		err = errors.New("no or unknown unit found")
		duration = 0
	}
	if duration == 0 {
		err = errors.New("duration for " + SurroundString(time, '"') + " is 0, please check your input")
	}
	return duration, err
}
