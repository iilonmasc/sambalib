package stringutil

import (
	"fmt"
	"gitlab.com/iilonmasc/sambalib/v3/sysutil"
	"testing"
)

func TestTimeConvert(t *testing.T) {
	fn := SurroundString(sysutil.FuncName(), '[')
	cases := []struct {
		description string
		in          string
		want        int
	}{
		{"Test time unit 'day' lowercase", "1d", 86400000},
		{"Test time unit 'day' uppercase", "1D", 86400000},
		{"Test time unit 'hour' lowercase", "1h", 3600000},
		{"Test time unit 'hour' uppercase", "1H", 3600000},
		{"Test time unit 'minute' lowercase", "10m", 600000},
		{"Test time unit 'minute' uppercase", "10M", 600000},
		{"Test time unit 'seconds' lowercase", "10s", 10000},
		{"Test time unit 'seconds' uppercase", "10S", 10000},
		{"Test time unit 'milliseconds' lowercase", "10ms", 10},
		{"Test time unit 'milliseconds' uppercase", "10MS", 10},
		{"Test time unit 'milliseconds' mixedcase", "10Ms", 10},
		{"Test time unit 'milliseconds' mixedcase", "10mS", 10},
		{"Test two mixed time units", "1m10s", 70000},
		{"Test three mixed time unit", "2h1m10s", 7270000},
		{"Test mixed time unit with space", "1m 10s", 70000},
		{"Test unknown time unit", "10xS", 0},
		{"Test empty time ", "", 0},
	}
	for _, c := range cases {
		got, _ := ConvertTime(c.in)
		if got != c.want {
			t.Errorf("%q %q Convert(%q) == %d, want %d", fn, c.description, c.in, got, c.want)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}
