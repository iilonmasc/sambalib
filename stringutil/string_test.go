package stringutil

import (
	"fmt"
	"gitlab.com/iilonmasc/sambalib/v3/sysutil"
	"testing"
)

func TestSurround(t *testing.T) {
	fn := SurroundString(sysutil.FuncName(), '[')
	type surroundInput struct {
		message string
		char    rune
	}
	cases := []struct {
		description string
		in          surroundInput
		want        string
	}{
		{"Surround string test with single-quotes", surroundInput{message: "test", char: '\''}, "'test'"},
		{"Surround empty string with single-quotes", surroundInput{"", '\''}, "''"},
		{"Surround string with curly-braces, '{' given", surroundInput{"", '{'}, "{}"},
		{"Surround string with curly-braces, '}' given", surroundInput{"", '}'}, "{}"},
		{"Surround string with paranthesis, '(' given", surroundInput{"", '('}, "()"},
		{"Surround string with paranthesis, ')' given", surroundInput{"", ')'}, "()"},
		{"Surround string with tags, '<' given", surroundInput{"", '<'}, "<>"},
		{"Surround string with tags, '>' given", surroundInput{"", '>'}, "<>"},
		{"Surround string with angular brackets, '[' given", surroundInput{"", '['}, "[]"},
		{"Surround string with angular brackets, ']' given", surroundInput{"", ']'}, "[]"},
	}
	for _, c := range cases {
		got := SurroundString(c.in.message, c.in.char)
		if got != c.want {
			t.Errorf("%q %q SurroundString(%q, %q) == %q, want %q", fn, c.description, c.in.message, c.in.char, got, c.want)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}
