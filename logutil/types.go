package logutil

// Logger logs messages.
type Logger struct {
	verbosity verbosity
	prefix    string
	rune rune
}

// Declare a new option setter func type.
type option func(*Logger)

// Verbosity levels for log messages.
type verbosity int
