package logutil

import (
	"fmt"
	"os"

	"gitlab.com/iilonmasc/sambalib/v3/stringutil"
)

const (
	// Default logging is Low (zero-value of an int).
	Critical = verbosity(iota)
	Warning
	All
)

var err error

// Prefix adds a string before a logging message.
func Prefix(s string) option {
	return func(logger *Logger) {
		logger.prefix = s
	}
}

// Prefix adds a string before a logging message.
func Rune(r rune) option {
	return func(logger *Logger) {
		logger.rune = r
	}
}

// SetOptions changes the options of Logger.
func (logger *Logger) SetOptions(opts ...option) {
	for _, applyOpt := range opts {
		applyOpt(logger)
	}
}

// LowVerbosity enables logging only the critical messages.
func LogLevel(level verbosity) option {
	return func(logger *Logger) {
		logger.verbosity = level
	}
}

// Critical logs when something bad has happened.
func (logger *Logger) Critical(s ...string) {
	msg := stringutil.Concatenate(s)
	logger.print("CRITICAL", msg, Critical)
}

// Info logs just for informational messages.
func (logger *Logger) Warning(s ...string) {
	if logger.verbosity > Critical {
		msg := stringutil.Concatenate(s)
		logger.print("WARNING", msg, Warning)
	}
}

// Info logs just for informational messages.
func (logger *Logger) Info(s ...string) {
	if logger.verbosity > Warning || logger.verbosity == All {
		msg := stringutil.Concatenate(s)
		logger.print("INFO", msg, All)
	}
}

func (logger *Logger) print(level, msg string, logLevel verbosity) {
	prefix := ""
	if logger.rune == 0 {
		logger.SetOptions(Rune('['))
	}

	if logger.prefix != "" {
		prefix = stringutil.SurroundString(logger.prefix, logger.rune)
	}
	if logLevel > Warning {
		_, err = fmt.Fprintf(os.Stdout, "%s %-10s: %s\n", prefix, level, msg)
		logger.CheckCritical(err, 1)
	} else {
		_, err = fmt.Fprintf(os.Stderr, "%s %-10s: %s\n", prefix, level, msg)
		logger.CheckCritical(err, 1)
	}
}

func (logger *Logger) CheckCritical(err error, exit int) {
	if err != nil {
		logger.Critical(err.Error())
		os.Exit(exit)
	}
}
func (logger *Logger) CheckWarning(err error) {
	if err != nil {
		logger.Warning(err.Error())
	}
}
func (logger *Logger) CheckInfo(err error) {
	if err != nil {
		logger.Info(err.Error())
	}
}
